document.addEventListener("DOMContentLoaded", function () {
  let elements = document.querySelectorAll(".carousel-element")
  elements.forEach(function (element, index) {
    if (index !== 0) {
      element.style.display = "none"
    }
  })
})
let firstButton = document.querySelector(".carousel-btn")
firstButton.classList.add("active")

function showText(event) {
  let elements = document.querySelectorAll(".carousel-element")
  elements.forEach(function (element) {
    element.style.display = "none"
  })

  let carouselButtons = document.querySelectorAll(".carousel-btn")
  carouselButtons.forEach(function (button) {
    button.classList.remove("active")
  })

  let index = Array.from(event.target.parentNode.children).indexOf(event.target)

  elements[index].style.display = "flex"

  event.target.classList.add("active")
}

let carouselButtons = document.querySelectorAll(".carousel-btn")
carouselButtons.forEach(function (button) {
  button.addEventListener("click", showText)
})

let imagePaths = [
  "./randomImages/img/Layer 1 5.png",
  "./randomImages/img/Layer 24.png",
  "./randomImages/img/Layer 25.png",
  "./randomImages/img/Layer 26.png",
  "./randomImages/img/Layer 27.png",
  "./randomImages/img/Layer 28.png",
  "./randomImages/img/Layer 29.png",
  "./randomImages/img/Layer 30.png",
  "./randomImages/img/Layer 31.png",
  "./randomImages/img/Layer 32.png",
  "./randomImages/img/Layer 33.png",
  "./randomImages/img/Layer 34.png",
]

function addImages() {
  for (let i = 1; i <= 12; i++) {
    let imageCollection = document.getElementById("work-exp-images")
    let elementDiv = document.createElement("div")
    elementDiv.classList.add("work-exp-image-element")
    let containerDiv = document.createElement("div")
    containerDiv.classList.add("work-exp-image-container")
    elementDiv.appendChild(containerDiv)
    let hoverDiv = document.createElement("div")
    hoverDiv.classList.add("work-hover-block")
    elementDiv.appendChild(hoverDiv)
    let iconDiv = document.createElement("div")
    iconDiv.classList.add("hover-block-icon")
    let icon = document.createElement("img")
    icon.src = "./img/icon.svg"
    iconDiv.appendChild(icon)
    hoverDiv.appendChild(iconDiv)
    let hoverParagraphDiv = document.createElement("div")
    hoverParagraphDiv.classList.add("hover-paragraphs")
    let hoverParagraph1 = document.createElement("p")
    hoverParagraph1.classList.add("hover-paragraph-1")
    hoverParagraph1.textContent = "creative design"
    hoverParagraphDiv.appendChild(hoverParagraph1)
    let hoverParagraph2 = document.createElement("p")
    hoverParagraph2.classList.add("hover-paragraph-2")
    hoverParagraph2.textContent = "Web design"
    hoverParagraphDiv.appendChild(hoverParagraph2)
    hoverDiv.appendChild(hoverParagraphDiv)
    imageCollection.appendChild(elementDiv)

    let img = document.createElement("img")
    img.classList.add("work-exp-image")
    containerDiv.appendChild(img)
    let randomIndex = Math.floor(Math.random() * imagePaths.length)
    img.src = imagePaths[randomIndex]
  }
  let loadMoreButton = document.getElementById("load-more-button")
  let clickCount = 2
  loadMoreButton.addEventListener("click", () => {
    clickCount++
    if (clickCount > 2) {
      loadMoreButton.style.display = "none"
    }
  })
}

const tabContent = document.getElementsByClassName("work-exp-image-element")
const newTabArray = Array.from(tabContent)

function hideImages(className, item) {
  document
    .querySelector(".work-exp-tab-active")
    .classList.remove("work-exp-tab-active")
  item.classList.add("work-exp-tab-active")
  const itemId = item.getAttribute("id")
  newTabArray.forEach((element) => {
    if (itemId === "all" || element.classList.contains(className)) {
      element.classList.remove("hidden-image")
    } else element.classList.toggle("hidden-image")
    // if (element.classList.contains(className)) {}
  })
}

document
  .querySelector(".work-exp-tab-ul")
  .addEventListener("click", (event) => {
    if (event.target.classList.contains("work-exp-tab")) {
      const targetAttribute = event.target.getAttribute("id")
      hideImages(targetAttribute, event.target)
    }
  })

// SLIDER

const [...trigger] = document.getElementsByClassName("people-switch-e")
const [...target] = document.getElementsByClassName("people-e")

const zero = document.getElementById("0")
const one = document.getElementById("1")
const two = document.getElementById("2")
const three = document.getElementById("3")

function display(element, value, visibility) {
  if (value === "id" && visibility === false) {
    element.style.display = "none"
  } else if (value === "id" && visibility === true) {
    element.style.display = "flex"
  } else if (value === "class" && visibility === false) {
    element.forEach((elt) => (elt.style.display = "none"))
  } else if (value === "class" && visibility === true) {
    element.forEach((elt) => (elt.style.display = "flex"))
  } else if (value === "element" && visibility === false) {
    element.style.display = "none"
  } else if (value === "element" && visibility === true) {
    element.style.display = "flex"
  }
}

function styleSetting(element, index, prop, value) {
  if (prop === "transition") {
    element[index].style.transition = value
  } else if (prop === "transform") {
    element[index].style.transform = value
  } else if (index === "css") {
    element.style.cssText = prop
  }
}

function findTargetSlide(value) {
  let result = value.getAttribute("id")
  return +result
}

function setAttr(elt) {
  return elt.classList.add("selected")
}

function removeAttrs(elt) {
  return elt.forEach((elt) => elt.classList.remove("selected"))
}

function checkAttr(elt) {
  return elt.classList.contains("selected")
}

display(two, "id", true)

trigger.forEach((elt) => (elt.style.transition = "transform 0.6s"))

styleSetting(trigger, 2, "transform", "translateY(-22%)")

setAttr(trigger[2])

trigger.forEach((elt, index, array) => {
  elt.addEventListener("click", () => {
    trigger.forEach((elt) => (elt.style.transform = "unset"))
    styleSetting(array, index, "transform", "translateY(-22%)")

    if (index === findTargetSlide(zero) && checkAttr(array[index]) === false) {
      target.forEach((elt) => {
        elt.style.transition = "transform 0.5s ease-in"
        elt.style.transform = "translate(200px)"
        setTimeout(() => (elt.style.display = "none"), 500)
      })

      setTimeout(() => display(zero, "id", true), 500)

      zero.style.transform = "translate(-200px)"

      setTimeout(() => {
        zero.style.transition = "transform 0.5s ease-in"
        zero.style.transform = "translate(1px)"
      }, 550)
    } else if (
      index === findTargetSlide(one) &&
      checkAttr(array[index]) === false
    ) {
      target.forEach((elt) => {
        elt.style.transition = "transform 0.5s ease-in"
        elt.style.transform = "translate(200px)"
        setTimeout(() => (elt.style.display = "none"), 500)
      })

      setTimeout(() => display(one, "id", true), 500)

      one.style.transform = "translate(-200px)"

      setTimeout(() => {
        one.style.transition = "transform 0.5s ease-in"
        one.style.transform = "translate(1px)"
      }, 550)
    } else if (
      index === findTargetSlide(two) &&
      checkAttr(array[index]) === false
    ) {
      target.forEach((elt) => {
        elt.style.transition = "transform 0.5s ease-in"
        elt.style.transform = "translate(200px)"
        setTimeout(() => (elt.style.display = "none"), 500)
      })

      setTimeout(() => display(two, "id", true), 500)

      two.style.transform = "translate(-200px)"

      setTimeout(() => {
        two.style.transition = "transform 0.5s ease-in"
        two.style.transform = "translate(1px)"
      }, 550)
    } else if (
      index === findTargetSlide(three) &&
      checkAttr(array[index]) === false
    ) {
      target.forEach((elt) => {
        elt.style.transition = "transform 0.5s ease-in"
        elt.style.transform = "translate(200px)"
        setTimeout(() => (elt.style.display = "none"), 500)
      })

      setTimeout(() => display(three, "id", true), 500)

      three.style.transform = "translate(-200px)"

      setTimeout(() => {
        three.style.transition = "transform 0.5s ease-in"
        three.style.transform = "translate(1px)"
      }, 550)
    }

    removeAttrs(array)
    setAttr(array[index])
  })
})

const leftClick = document.getElementById("left-click")
const rightClick = document.getElementById("right-click")
leftClick.style.outline = "none"
rightClick.style.outline = "none"

leftClick.addEventListener("click", (ev) => {
  if (trigger[2].classList.contains("selected")) {
    trigger.forEach((e) => (e.style.transform = "unset"))
    removeAttrs(trigger)
    trigger[1].style.transform = "translateY(-22%)"
    trigger[1].classList.add("selected")

    // ------------------------------------------

    target.forEach((elt) => {
      elt.style.transition = "transform 0.5s ease-in"
      elt.style.transform = "translate(200px)"
      setTimeout(() => (elt.style.display = "none"), 500)
    })

    setTimeout(() => display(one, "id", true), 500)

    one.style.transform = "translate(-200px)"

    setTimeout(() => {
      one.style.transition = "transform 0.5s ease-in"
      one.style.transform = "translate(1px)"
    }, 550)
    return
  }

  if (trigger[3].classList.contains("selected")) {
    removeAttrs(trigger)
    trigger.forEach((e) => (e.style.transform = "unset"))
    trigger[2].style.transform = "translateY(-22%)"
    trigger[2].classList.add("selected")

    // -------------------------------------

    target.forEach((elt) => {
      elt.style.transition = "transform 0.5s ease-in"
      elt.style.transform = "translate(200px)"
      setTimeout(() => (elt.style.display = "none"), 500)
    })

    setTimeout(() => display(two, "id", true), 500)

    two.style.transform = "translate(-200px)"

    setTimeout(() => {
      two.style.transition = "transform 0.5s ease-in"
      two.style.transform = "translate(1px)"
    }, 550)
    return
  }

  if (trigger[0].classList.contains("selected")) {
    trigger.forEach((e) => (e.style.transform = "unset"))
    removeAttrs(trigger)
    trigger[3].style.transform = "translateY(-22%)"
    trigger[3].classList.add("selected")

    // -----------------------------------------------

    target.forEach((elt) => {
      elt.style.transition = "transform 0.5s ease-in"
      elt.style.transform = "translate(200px)"
      setTimeout(() => (elt.style.display = "none"), 500)
    })

    setTimeout(() => display(three, "id", true), 500)

    three.style.transform = "translate(-200px)"

    setTimeout(() => {
      three.style.transition = "transform 0.5s ease-in"
      three.style.transform = "translate(1px)"
    }, 550)
    return
  }

  if (trigger[1].classList.contains("selected")) {
    trigger.forEach((e) => (e.style.transform = "unset"))
    removeAttrs(trigger)
    trigger[0].style.transform = "translateY(-22%)"
    trigger[0].classList.add("selected")

    // ------------------------------------------------------------
    target.forEach((elt) => {
      elt.style.transition = "transform 0.5s ease-in"
      elt.style.transform = "translate(200px)"
      setTimeout(() => (elt.style.display = "none"), 500)
    })

    setTimeout(() => display(zero, "id", true), 500)

    zero.style.transform = "translate(-200px)"

    setTimeout(() => {
      zero.style.transition = "transform 0.5s ease-in"
      zero.style.transform = "translate(1px)"
    }, 550)
  }
})

rightClick.addEventListener("click", (ev) => {
  if (trigger[1].classList.contains("selected")) {
    trigger.forEach((e) => (e.style.transform = "unset"))
    removeAttrs(trigger)
    trigger[2].style.transform = "translateY(-22%)"
    trigger[2].classList.add("selected")

    // ------------------------------------------------------------

    target.forEach((elt) => {
      elt.style.transition = "transform 0.5s ease-in"
      elt.style.transform = "translate(200px)"
      setTimeout(() => (elt.style.display = "none"), 500)
    })

    setTimeout(() => display(two, "id", true), 500)

    two.style.transform = "translate(-200px)"

    setTimeout(() => {
      two.style.transition = "transform 0.5s ease-in"
      two.style.transform = "translate(1px)"
    }, 550)
    return
  }

  if (trigger[0].classList.contains("selected")) {
    trigger.forEach((e) => (e.style.transform = "unset"))
    removeAttrs(trigger)
    trigger[1].style.transform = "translateY(-22%)"
    // trigger[0].classList.remove("ready");
    trigger[1].classList.add("selected")

    // -----------------------------------------------

    target.forEach((elt) => {
      elt.style.transition = "transform 0.5s ease-in"
      elt.style.transform = "translate(200px)"
      setTimeout(() => (elt.style.display = "none"), 500)
    })

    setTimeout(() => display(one, "id", true), 500)

    one.style.transform = "translate(-200px)"

    setTimeout(() => {
      one.style.transition = "transform 0.5s ease-in"
      one.style.transform = "translate(1px)"
    }, 550)
    return
  }

  if (trigger[3].classList.contains("selected")) {
    removeAttrs(trigger)
    trigger.forEach((e) => (e.style.transform = "unset"))
    trigger[0].style.transform = "translateY(-22%)"
    trigger[0].classList.add("selected")
    // -------------------------------------

    target.forEach((elt) => {
      elt.style.transition = "transform 0.5s ease-in"
      elt.style.transform = "translate(200px)"
      setTimeout(() => (elt.style.display = "none"), 500)
    })

    setTimeout(() => display(zero, "id", true), 500)

    zero.style.transform = "translate(-200px)"

    setTimeout(() => {
      zero.style.transition = "transform 0.5s ease-in"
      zero.style.transform = "translate(1px)"
    }, 550)
    return
  }

  if (trigger[2].classList.contains("selected")) {
    trigger.forEach((e) => (e.style.transform = "unset"))
    removeAttrs(trigger)
    trigger[3].style.transform = "translateY(-22%)"
    trigger[3].classList.add("selected")

    target.forEach((elt) => {
      elt.style.transition = "transform 0.5s ease-in"
      elt.style.transform = "translate(200px)"
      setTimeout(() => (elt.style.display = "none"), 500)
    })

    setTimeout(() => display(three, "id", true), 500)

    three.style.transform = "translate(-200px)"

    setTimeout(() => {
      three.style.transition = "transform 0.5s ease-in"
      three.style.transform = "translate(1px)"
    }, 550)
    return
  }
})
